# [3.0.0](https://gitlab.com/opencloudengineer/rlz/compare/v2.1.0...v3.0.0) (2020-11-23)


### Features

* rm Dockerfile ([5b33544](https://gitlab.com/opencloudengineer/rlz/commit/5b335448d1c4d6e7327bdf1c215590e7a3152afa))


### BREAKING CHANGES

* rlz is a hidden job now

# [2.1.0](https://gitlab.com/opencloudengineer/rlz/compare/v2.0.0...v2.1.0) (2020-08-13)


### Features

* **ci:** rlz only on push  ([d0a448b](https://gitlab.com/opencloudengineer/rlz/commit/d0a448b74062202af33c94abcfece4c9fda406c7))

# [2.0.0](https://gitlab.com/opencloudengineer/rlz/compare/v1.7.0...v2.0.0) (2020-08-10)


### Code Refactoring

* **ci:** fine tune release ci yml template ([880de30](https://gitlab.com/opencloudengineer/rlz/commit/880de306214ee7d7cd4a57b47c95f8780372310f))


### BREAKING CHANGES

* **ci:** make template extensible without forking

# [1.7.0](https://gitlab.com/opencloudengineer/rlz/compare/v1.6.0...v1.7.0) (2020-08-10)


### Features

* not pre-release but a separate channel on beta and alpha ([17690b3](https://gitlab.com/opencloudengineer/rlz/commit/17690b39d907c13d67397f584037286c39fc0ec2))

# [1.6.0](https://gitlab.com/opencloudengineer/rlz/compare/v1.5.1...v1.6.0) (2020-08-09)


### Features

* branches config in release yml ([6f1985c](https://gitlab.com/opencloudengineer/rlz/commit/6f1985c0161b83ce514fdc73c89f010dbac9b226))

## [1.5.1](https://gitlab.com/opencloudengineer/rlz/compare/v1.5.0...v1.5.1) (2020-08-04)


### Bug Fixes

* from oc.dev to opencloudengineer ([16f7361](https://gitlab.com/opencloudengineer/rlz/commit/16f73615ee4a2f7cdc7ecedb6e52df86f82ecb79))

# [1.5.0](https://gitlab.com/oc.dev/rlz/compare/v1.4.1...v1.5.0) (2020-08-04)


### Features

* use npm plugin when package.json exists in the repo ([c8a4dc5](https://gitlab.com/oc.dev/rlz/commit/c8a4dc5532aec48a1e9a14ff2b95fd391a399747))

## [1.4.1](https://gitlab.com/oc.dev/rlz/compare/v1.4.0...v1.4.1) (2020-08-04)


### Bug Fixes

* rm cache keyword in publish stage ([6d7d281](https://gitlab.com/oc.dev/rlz/commit/6d7d281e5389911a2dbed326edf588960e3d1531))

# [1.4.0](https://gitlab.com/oc.dev/rlz/compare/v1.3.0...v1.4.0) (2020-08-04)


### Bug Fixes

* ' in Don't ([bb367d6](https://gitlab.com/oc.dev/rlz/commit/bb367d615947ca44579c2df8390e425e21b6b403))
* add ; ([faf4278](https://gitlab.com/oc.dev/rlz/commit/faf42782e7fcd7e8c43a09424ccbaf7b057e18fb))


### Features

* add semantic-release globally in Dockerfile ([f91a52f](https://gitlab.com/oc.dev/rlz/commit/f91a52f84af1663b5dd4818c83d7e531502d75d1))

# [1.3.0](https://gitlab.com/oc.dev/rlz/compare/v1.2.0...v1.3.0) (2020-08-04)


### Features

* releaserc simplified && in yml now ([f4527ea](https://gitlab.com/oc.dev/rlz/commit/f4527eac392505c89178932ab5d18770192a1e64))

# [1.2.0](https://gitlab.com/oc.dev/rlz/compare/v1.1.0...v1.2.0) (2020-08-03)


### Features

* add .releaserc template which is used in this project as well ([44799c1](https://gitlab.com/oc.dev/rlz/commit/44799c1824484f81857e3ac1a795f609b18cb2ce))

# [1.1.0](https://gitlab.com/oc.dev/rlz/compare/v1.0.0...v1.1.0) (2020-08-02)


### Features

* add Dockerfile and build ci ([f32e3df](https://gitlab.com/oc.dev/rlz/commit/f32e3df60d333630a49b32f22070e3c501db4567))

# 1.0.0 (2020-08-02)


### Features

* add semantic-release CI template ([bd98339](https://gitlab.com/oc.dev/rlz/commit/bd9833955ae4736928368cd9611cb85dde7629c9))
* add stage name in .gitlab-ci.yml ([b31ff3e](https://gitlab.com/oc.dev/rlz/commit/b31ff3e945b4d1f8e7c0bfc89239520b18a739f4))
* rm npm plugin in .releaserc ([b6b1a75](https://gitlab.com/oc.dev/rlz/commit/b6b1a753bbc2b52f0dcf4acbace813dbeb4cfd3c))
