# rlz

Fully automated version management and package publishing with Gitlab CI

## How to use :

Include in the .gitlab-ci.yml

```yaml
stages:
  - release

publish:
  stage: release
  extends: .rlz

include:
  - project: 'opencloudengineer/rlz'
    file: 'ci.yml'
```


[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
